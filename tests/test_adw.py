import pytest
from adw import AppDirs
from pathlib import Path


@pytest.fixture
def app_dirs():
    _dirs = AppDirs("adw_test_obj")

    yield _dirs

    def delete_path(path):
        if path.exists():
            path.rmdir()

    delete_path(_dirs.user.data_dir)
    delete_path(_dirs.user.config_dir)
    delete_path(_dirs.user.log_dir)
    delete_path(_dirs.user.state_dir)
    delete_path(_dirs.user.cache_dir)
    delete_path(_dirs.site.data_dir)
    delete_path(_dirs.site.config_dir)


def test_adw_user_dirs(app_dirs):
    print(app_dirs.user.to_string())
    assert isinstance(app_dirs.user.data_dir, Path)
    assert isinstance(app_dirs.user.config_dir, Path)
    assert isinstance(app_dirs.user.log_dir, Path)
    assert isinstance(app_dirs.user.state_dir, Path)
    assert isinstance(app_dirs.user.cache_dir, Path)


def test_adw_site_dirs(app_dirs):
    print(app_dirs.site.to_string())
    assert isinstance(app_dirs.user.data_dir, Path)
    assert isinstance(app_dirs.user.config_dir, Path)


def test_adw_assert_user_directories(app_dirs):
    assert not app_dirs.user.data_dir.exists()
    assert not app_dirs.user.config_dir.exists()
    assert not app_dirs.user.log_dir.exists()
    assert not app_dirs.user.state_dir.exists()
    assert not app_dirs.user.cache_dir.exists()

    app_dirs.user.assert_directories()

    assert app_dirs.user.data_dir.exists()
    assert app_dirs.user.config_dir.exists()
    assert app_dirs.user.log_dir.exists()
    assert app_dirs.user.state_dir.exists()
    assert app_dirs.user.cache_dir.exists()


@pytest.mark.skip("Must run with admin priviledges for it to work")
def test_adw_assert_site_directories(app_dirs):
    assert not app_dirs.site.data_dir.exists()
    assert not app_dirs.site.config_dir.exists()

    app_dirs.user.assert_directories()

    assert app_dirs.site.data_dir.exists()
    assert app_dirs.site.config_dir.exists()
